#!/usr/bin/python3
from struct import pack,unpack
from decimal import *
"""
Implements MAL Attribute Encoder/Decoder as specified in the MAL SPP Binding and Encoder:
https://public.ccsds.org/Pubs/524x1b1.pdf
"""

_PRESENT = (1).to_bytes(1, byteorder='big', signed=False)
_NOT_PRESENT = (0).to_bytes(1, byteorder='big', signed=False)
_TRUE = _PRESENT
_FALSE = _NOT_PRESENT

DAYS_FROM_CCSDS_TO_UNIX_EPOCH = 4383
SECONDS_FROM_CCSDS_TO_UNIX_EPOCH = 378691200
MILLISECONDS_FROM_CCSDS_TO_UNIX_EPOCH = 378691200000
NANOSECONDS_FROM_CCSDS_TO_UNIX_EPOCH = MILLISECONDS_FROM_CCSDS_TO_UNIX_EPOCH * 1000000;
SECONDS_IN_DAY = 86400
MILLISECONDS_IN_DAY = 86400000
NANOSECONDS_IN_DAY = MILLISECONDS_IN_DAY * 1000000;

ElementRegistry = None # iniated at the end of the module, needed to allow interpreter to proceed
AttributeRegistry = None # same as above

class Element():
    SHORT_FORM_PART = -1
    _AREA = -1
    _SERVICE = -1
    _AREA_VERSION = -1
    ENCODED_ELEMENT_TYPE = -1
    _SIZE = 0

    @classmethod
    def encode(cls,value):
        raise NotImplementedError

    @classmethod
    def decode(cls,data):
        v,s = ULong.decode(data)
        elemClass = ElementRegistry[v]
        ev,es = elemClass.decode(data[8:])
        return elemClass,ev,es+s # class, value, total size

    def as_null(self):
        return _NOT_PRESENT

    def as_nullable(self):
        self[0:0] = _PRESENT
        return self

    def as_attribute(self):
        self[0:0] = self.__class__.ENCODED_SHORT_FORM_PART # mutable way to prepend short form part
        return self

    def as_element(self):
        self[0:0] = self.__class__.ENCODED_ELEMENT_TYPE # mutable way to prepend element type
        return self

    def as_composite(self):
        return self.as_element()

    def size(self):
        return self.__class__._SIZE


class MetaNullable:
    NULL = _NOT_PRESENT
    wrappedClass = None

    @classmethod
    def encode(cls,value = None):
        if value == None:
            return _NOT_PRESENT
        return _PRESENT + cls.wrappedClass.encode(value)

    @classmethod
    def decode(cls,data):
        if data[0] == 0:
            return None,1
        v,s = cls.wrappedClass.decode(data[1:])
        return v,s+1

    @classmethod
    def isNull(cls,data):
        return (self[0]==0)

    @classmethod
    def isPresent(cls,data):
        return not cls.isNull()


def Nullable(cls):
    """
    Creates a wrapper for the provided class that is nullable.
    Usage: Nullable(Boolean)
    Effect: Creates a NullableBoolean class, NullableBoolean.encode will a boolean as Nullable
    """
    return type(
        "Nullable" + cls.__name__, #class name e.g. NullableBoolean
        (MetaNullable,cls),
        {"wrappedClass":cls,
        "encode":MetaNullable.encode,
        "decode":MetaNullable.decode}) # overrides the class specific wrappedClass


class Composite(Element):
    pass

class List:
    def __init__(self,obj_cls):
        self.elements = []
        self.element_types = []
        self.element_class = obj_cls
        self.__name__ = obj_cls.__name__ + "List"
        if (   self.element_class == Element
            or self.element_class == Attribute # attributes are encoded as Elements when in lists
            or self.element_class == Composite
            or issubclass(self.element_class,Composite)):
            self.encode_as_element = True
        else:
            self.encode_as_element = False

    def append(self,obj,ctype = None):
        if self.encode_as_element == True and ctype == None:
            raise TypeError("Cannot encode as element if element type is not provided")
        if self.encode_as_element == False and ctype != None and ctype != self.element_class:
            raise TypeError("Class mismatch, expected ctype undefined or "+self.element_class )

        if ctype == None:
            self.element_types.append(self.element_class)
        else:
            self.element_types.append(ctype)
        self.elements.append(obj)

    def extend(self,obj,ctype = None):
        if ctype == None:
            self.element_types.append(self.element_class)
        else:
            self.element_types.append(ctype)
        self.elements.extend(obj)

    def encode(self):
        out = UInteger.encode(len(self.elements)) # encodes size of the list
        for elemIdx in range(len(self.elements)): # iterate through the elements
            elem = self.elements[elemIdx]
            elem_class = self.element_types[elemIdx]
            if(self.encode_as_element):
                out += elem_class.encode(elem).as_element().as_nullable()
            else:
                out += self.element_class.encode(elem).as_nullable() # all elements of a list are encoded as nullables
        return out

class Attribute(Element):
    pass

class Blob(Attribute,bytearray):
    """
    The Blob structure is used to store binary object attributes. It is a variable-length, unbounded, octet array. The distinction between this type and a list of Octet attributes is that this type may allow language mappings and encodings to use more efficient or appropriate representations.
    """
    _AREA = 1
    _SERVICE = 0
    _AREA_VERSION = 1
    SHORT_FORM_PART = 1
    ENCODED_SHORT_FORM_PART =  b'\x00'
    DECODED_ELEMENT_TYPE = 0x0001000001000001
    ENCODED_ELEMENT_TYPE = b'\x00\x01\x00\x00\x01\x00\x00\x01'
    _SIZE = 4

    @classmethod
    def decode(cls,data):
        blobSize = int.from_bytes(data[0:4], byteorder='big', signed=False)
        offset = blobSize + 4
        return Blob(data[4:offset])

    def encode(self):
        blobSize = (len(self)).to_bytes(4, byteorder='big', signed=False)
        return (blobSize + self)

    def size(self):
        return  _SIZE + len(self)


class Boolean(Attribute,int):
    """
    The Boolean structure is used to store Boolean attributes. Possible values are 'True' or 'False'.
    """
    _AREA = 1
    _SERVICE = 0
    _AREA_VERSION = 1
    SHORT_FORM_PART = 2
    ENCODED_SHORT_FORM_PART =  b'\x01'
    DECODED_ELEMENT_TYPE = 0x0001000001000002
    ENCODED_ELEMENT_TYPE = b'\x00\x01\x00\x00\x01\x00\x00\x02'
    _SIZE = 1

    @classmethod
    def decode(cls,data):
        return Boolean(bool(data[0]))

    def encode(self):
        if bool(self):
            return _TRUE
        return _FALSE


class Duration(Attribute,float):
    """
    The Duration structure is used to store Duration attributes. It represents a length of time in seconds. It may contain a fractional component.
    """
    _AREA = 1
    _SERVICE = 0
    _AREA_VERSION = 1
    SHORT_FORM_PART = 3
    ENCODED_SHORT_FORM_PART =  b'\x02'
    DECODED_ELEMENT_TYPE = 0x0001000001000003
    ENCODED_ELEMENT_TYPE = b'\x00\x01\x00\x00\x01\x00\x00\x03'
    _SIZE = 8

    @classmethod
    def decode(cls,data):
        """
        Defaults to double
        """
        return unpack(">d", data[0:8])[0], 8

    def encode(self):
        """
        Defaults to double
        """
        return bytearray(pack(">d", self))


class Float(Attribute,float):
    """
    The Float structure is used to store floating point attributes using the IEEE 754 32-bit range.
    Three special values exist for this type: POSITIVE_INFINITY, NEGATIVE_INFINITY, and NaN (Not A Number).
    """
    _AREA = 1
    _SERVICE = 0
    _AREA_VERSION = 1
    SHORT_FORM_PART = 4
    ENCODED_SHORT_FORM_PART =  b'\x03'
    DECODED_ELEMENT_TYPE = 0x0001000001000004
    ENCODED_ELEMENT_TYPE = b'\x00\x01\x00\x00\x01\x00\x00\x04'
    _SIZE = 4

    @classmethod
    def decode(cls,data):
        return unpack(">f", data[0:4])[0], 4

    def encode(self):
        return bytearray(pack(">f", float(self)))


class Double(Attribute,float):
    """
    The Double structure is used to store floating point attributes using the IEEE 754 64-bit range.
    Three special values exist for this type: POSITIVE_INFINITY, NEGATIVE_INFINITY, and NaN (Not A Number).
    """
    _AREA = 1
    _SERVICE = 0
    _AREA_VERSION = 1
    SHORT_FORM_PART = 5
    ENCODED_SHORT_FORM_PART =  b'\x04'
    DECODED_ELEMENT_TYPE = 0x0001000001000005
    ENCODED_ELEMENT_TYPE = b'\x00\x01\x00\x00\x01\x00\x00\x05'
    _SIZE = 8

    @classmethod
    def decode(cls,data):
        return unpack(">d", data[0:8])[0], 8

    def encode(self):
        return bytearray(pack(">d", self))


class Identifier(Attribute,str):
    """
    The Identifier structure is used to store an identifier and can be used for indexing. It is a variable-length, unbounded, Unicode string.
    """
    _AREA = 1
    _SERVICE = 0
    _AREA_VERSION = 1
    SHORT_FORM_PART = 6
    ENCODED_SHORT_FORM_PART =  b'\x05'
    DECODED_ELEMENT_TYPE = 0x0001000001000006
    ENCODED_ELEMENT_TYPE = b'\x00\x01\x00\x00\x01\x00\x00\x06'
    _SIZE = 4

    @classmethod
    def decode(cls,data):
        strlen,n = UInteger.decode(data[0:4])
        payload = data[4:(strlen+4)].decode('utf-8')
        return payload,strlen+4

    def encode(self):
        strlen = UInteger.encode(len(self))
        payload = bytearray(self, 'utf-8')
        return (strlen + payload)


class Octet(Attribute,int):
    """
    The Octet structure is used to store 8-bit signed attributes. The permitted range is -128 to 127.
    """
    _AREA = 1
    _SERVICE = 0
    _AREA_VERSION = 1
    SHORT_FORM_PART = 7
    ENCODED_SHORT_FORM_PART =  b'\x06'
    DECODED_ELEMENT_TYPE = 0x0001000001000007
    ENCODED_ELEMENT_TYPE = b'\x00\x01\x00\x00\x01\x00\x00\x07'
    _SIZE = 1

    @classmethod
    def decode(cls,data):
        return int.from_bytes(data[0:1], byteorder='big', signed=True) 

    def encode(self):
        return int(self).to_bytes(1, byteorder='big', signed=True)


class UOctet(Attribute,int):
    """
    The UOctet structure is used to store 8-bit unsigned attributes. The permitted range is 0 to 255.
    """
    _AREA = 1
    _SERVICE = 0
    _AREA_VERSION = 1
    SHORT_FORM_PART = 8
    ENCODED_SHORT_FORM_PART =  b'\x07'
    DECODED_ELEMENT_TYPE = 0x0001000001000008
    ENCODED_ELEMENT_TYPE = b'\x00\x01\x00\x00\x01\x00\x00\x08'
    _SIZE = 1

    @classmethod
    def decode(cls,data):
        return int.from_bytes(data[0:1], byteorder='big', signed=False) 

    def encode(self):
        return int(self).to_bytes(1, byteorder='big', signed=False)


class Short(Attribute,int):
    """
    The Short structure is used to store 16-bit signed attributes. The permitted range is -32768 to 32767.
    """
    _AREA = 1
    _SERVICE = 0
    _AREA_VERSION = 1
    SHORT_FORM_PART = 9
    ENCODED_SHORT_FORM_PART =  b'\x08'
    DECODED_ELEMENT_TYPE = 0x0001000001000009
    ENCODED_ELEMENT_TYPE = b'\x00\x01\x00\x00\x01\x00\x00\x09'
    _SIZE = 2

    @classmethod
    def decode(cls,data):
        return int.from_bytes(data[0:2], byteorder='big', signed=True) 

    def encode(self):
        return int(self).to_bytes(2, byteorder='big', signed=True)


class UShort(Attribute,int):
    """
    The UShort structure is used to store 16-bit unsigned attributes. The permitted range is 0 to 65535.
    """
    _AREA = 1
    _SERVICE = 0
    _AREA_VERSION = 1
    SHORT_FORM_PART = 10
    ENCODED_SHORT_FORM_PART =  b'\x09'
    DECODED_ELEMENT_TYPE = 0x000100000100000A
    ENCODED_ELEMENT_TYPE = b'\x00\x01\x00\x00\x01\x00\x00\x0A'
    _SIZE = 2

    @classmethod
    def decode(cls,data):
        return int.from_bytes(data[0:2], byteorder='big', signed=False) 

    def encode(self):
        return int(self).to_bytes(2, byteorder='big', signed=False)


class Integer(Attribute,int):
    """
    The Integer structure is used to store 32-bit signed attributes. The permitted range is -2147483648 to 2147483647.
    """
    _AREA = 1
    _SERVICE = 0
    _AREA_VERSION = 1
    SHORT_FORM_PART = 11
    ENCODED_SHORT_FORM_PART =  b'\x0A'
    DECODED_ELEMENT_TYPE = 0x000100000100000B
    ENCODED_ELEMENT_TYPE = b'\x00\x01\x00\x00\x01\x00\x00\x0B'
    _SIZE = 4

    @classmethod
    def decode(cls,data):
        return int.from_bytes(data[0:4], byteorder='big', signed=True) 

    def encode(self):
        return int(self).to_bytes(4, byteorder='big', signed=True)


class UInteger(Attribute,int):
    """
    The UInteger structure is used to store 32-bit unsigned attributes. The permitted range is 0 to 4294967295.
    """
    _AREA = 1
    _SERVICE = 0
    _AREA_VERSION = 1
    SHORT_FORM_PART = 12
    ENCODED_SHORT_FORM_PART =  b'\x0B'
    DECODED_ELEMENT_TYPE = 0x000100000100000C
    ENCODED_ELEMENT_TYPE = b'\x00\x01\x00\x00\x01\x00\x00\x0C'
    _SIZE = 4

    @classmethod
    def decode(cls,data):
        return int.from_bytes(data[0:4], byteorder='big', signed=False) 

    def encode(self):
        return int(self).to_bytes(4, byteorder='big', signed=False)


class Long(Attribute,int):
    """
    The Long structure is used to store 64-bit signed attributes. The permitted range is -9223372036854775808 to 9223372036854775807.
    """
    _AREA = 1
    _SERVICE = 0
    _AREA_VERSION = 1
    SHORT_FORM_PART = 13
    ENCODED_SHORT_FORM_PART =  b'\x0C'
    DECODED_ELEMENT_TYPE = 0x000100000100000D
    ENCODED_ELEMENT_TYPE = b'\x00\x01\x00\x00\x01\x00\x00\x0D'
    _SIZE = 8

    @classmethod
    def decode(cls,data):
        return int.from_bytes(data[0:8], byteorder='big', signed=True) 

    def encode(self):
        return int(self).to_bytes(8, byteorder='big', signed=True)


class ULong(Attribute,int):
    """
    The ULong structure is used to store 64-bit unsigned attributes. The permitted range is 0 to 18446744073709551615.
    """
    _AREA = 1
    _SERVICE = 0
    _AREA_VERSION = 1
    SHORT_FORM_PART = 14
    ENCODED_SHORT_FORM_PART =  b'\x0D'
    DECODED_ELEMENT_TYPE = 0x000100000100000E
    ENCODED_ELEMENT_TYPE = b'\x00\x01\x00\x00\x01\x00\x00\x0E'
    _SIZE = 8

    @classmethod
    def decode(cls,data):
        return int.from_bytes(data[0:8], byteorder='big', signed=False) 

    def encode(self):
        return int(self).to_bytes(8, byteorder='big', signed=False)


class String(Attribute,str):
    """
    The String structure is used to store String attributes. It is a variable-length, unbounded, Unicode string.
    """
    _AREA = 1
    _SERVICE = 0
    _AREA_VERSION = 1
    SHORT_FORM_PART = 15
    ENCODED_SHORT_FORM_PART =  b'\x0E'
    DECODED_ELEMENT_TYPE = 0x000100000100000F
    ENCODED_ELEMENT_TYPE = b'\x00\x01\x00\x00\x01\x00\x00\x0F'
    _SIZE = 4

    @classmethod
    def decode(cls,data):
        strlen,n = UInteger.decode(data[0:4])
        payload = data[4:(strlen+4)].decode('utf-8')
        return payload,strlen+4

    def encode(self):
        strlen = UInteger.encode(len(self))
        payload = bytearray(self, 'utf-8')
        return (strlen + payload)


class Time(Attribute,float):
    """
    The Time structure is used to store absolute time attributes. It represents an absolute date and time to millisecond resolution.
    """
    _AREA = 1
    _SERVICE = 0
    _AREA_VERSION = 1
    SHORT_FORM_PART = 16
    ENCODED_SHORT_FORM_PART =  b'\x0F'
    DECODED_ELEMENT_TYPE = 0x0001000001000010
    ENCODED_ELEMENT_TYPE = b'\x00\x01\x00\x00\x01\x00\x00\x10'
    _SIZE = 6

    @classmethod
    def decode(cls,data):
        days = int.from_bytes(data[0:2], byteorder='big', signed=False) - DAYS_FROM_CCSDS_TO_UNIX_EPOCH
        seconds = days * SECONDS_IN_DAY 
        ms = int.from_bytes(data[2:6], byteorder='big', signed=False) /1000
        return seconds + ms , 6 

    def encode(self):
        """
        It implements CCSDS Day Segmented Time Code (CDS) with P-field assumed to be "01000000"(16 bit day segment, 32 bit ms of day segment)
        """
        _days = (self + SECONDS_FROM_CCSDS_TO_UNIX_EPOCH) / SECONDS_IN_DAY
        days = int(_days).to_bytes(2, byteorder='big', signed=False)
        _ms = (self*1000) % MILLISECONDS_IN_DAY
        ms = int(_ms).to_bytes(4, byteorder='big', signed=False)
        return days + ms


class FineTime(Attribute,Decimal):
    """
    The FineTime structure is used to store high-resolution absolute time attributes. It represents an absolute date and time to picosecond resolution.
    """
    _AREA = 1
    _SERVICE = 0
    _AREA_VERSION = 1
    SHORT_FORM_PART = 17
    ENCODED_SHORT_FORM_PART =  b'\x10'
    DECODED_ELEMENT_TYPE = 0x0001000001000011
    ENCODED_ELEMENT_TYPE = b'\x00\x01\x00\x00\x01\x00\x00\x11'
    _SIZE = 10

    @classmethod
    def decode(cls,data):
        days = int.from_bytes(data[0:2], byteorder='big', signed=False) - DAYS_FROM_CCSDS_TO_UNIX_EPOCH
        seconds = days * SECONDS_IN_DAY 
        ms = Decimal(int.from_bytes(data[2:6], byteorder='big', signed=False)) /1000
        ps = Decimal(int.from_bytes(data[6:10], byteorder='big', signed=False)) /(1000 * 1000000000)
        return Decimal(seconds) + ms + ps , 10 

    def encode(self):
        """
        It implements CCSDS Day Segmented Time Code (CDS) with P-field assumed to be "01000010" (16 bit day segment, 32 bit ms of day segment, 32 bit sub-ms segment)
        """
        _days = (self + SECONDS_FROM_CCSDS_TO_UNIX_EPOCH) / SECONDS_IN_DAY
        _ms = (self * 1000) % MILLISECONDS_IN_DAY
        _ps = ((self * 1000) % 1) * 1000000000 # picoseconds *in* miliseconds

        days = int(_days).to_bytes(2, byteorder='big', signed=False)
        ms = int(_ms).to_bytes(4, byteorder='big', signed=False)
        ps = int(_ps).to_bytes(4, byteorder='big', signed=False)

        return (days + ms + ps)


class URI(Attribute,str):
    """
    The URI structure is used to store URI addresses. It is a variable-length, unbounded, Unicode string.
    """
    _AREA = 1
    _SERVICE = 0
    _AREA_VERSION = 1
    SHORT_FORM_PART = 18
    ENCODED_SHORT_FORM_PART =  b'\x11'
    DECODED_ELEMENT_TYPE = 0x0001000001000012
    ENCODED_ELEMENT_TYPE = b'\x00\x01\x00\x00\x01\x00\x00\x12'
    _SIZE = 4

    # TODO: encode_uri


# Below module level variables are intialized

AttributeRegistry = {
    Blob.SHORT_FORM_PART : Blob,
    Boolean.SHORT_FORM_PART : Boolean,
    Duration.SHORT_FORM_PART : Duration,
    Float.SHORT_FORM_PART : Float,
    Double.SHORT_FORM_PART : Double,
    Identifier.SHORT_FORM_PART : Identifier,
    Octet.SHORT_FORM_PART : Octet,
    UOctet.SHORT_FORM_PART : UOctet,
    Short.SHORT_FORM_PART : Short,
    UShort.SHORT_FORM_PART : UShort,
    Integer.SHORT_FORM_PART : Integer,
    UInteger.SHORT_FORM_PART : UInteger,
    Long.SHORT_FORM_PART : Long,
    ULong.SHORT_FORM_PART : ULong,
    String.SHORT_FORM_PART : String,
    Time.SHORT_FORM_PART : Time,
    FineTime.SHORT_FORM_PART : FineTime,
    URI.SHORT_FORM_PART : URI,
}

ElementRegistry = {
    Blob.DECODED_ELEMENT_TYPE : Blob,
    Boolean.DECODED_ELEMENT_TYPE : Boolean,
    Duration.DECODED_ELEMENT_TYPE : Duration,
    Float.DECODED_ELEMENT_TYPE : Float,
    Double.DECODED_ELEMENT_TYPE : Double,
    Identifier.DECODED_ELEMENT_TYPE : Identifier,
    Octet.DECODED_ELEMENT_TYPE : Octet,
    UOctet.DECODED_ELEMENT_TYPE : UOctet,
    Short.DECODED_ELEMENT_TYPE : Short,
    UShort.DECODED_ELEMENT_TYPE : UShort,
    Integer.DECODED_ELEMENT_TYPE : Integer,
    UInteger.DECODED_ELEMENT_TYPE : UInteger,
    Long.DECODED_ELEMENT_TYPE : Long,
    ULong.DECODED_ELEMENT_TYPE : ULong,
    String.DECODED_ELEMENT_TYPE : String,
    Time.DECODED_ELEMENT_TYPE : Time,
    FineTime.DECODED_ELEMENT_TYPE : FineTime,
    URI.DECODED_ELEMENT_TYPE : URI,
}
