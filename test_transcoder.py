#!/usr/bin/python3

import pytest
import unittest
from parameterized import parameterized
from transcoder.attributes import *

class TestMe(unittest.TestCase):

    def test_encode_3(self):
        print("Encoding 3")
        a = Integer.encode(3)
        print("Encoded as " + str(a))
        b,c = Integer.decode(a)
        print("Decoded as " + str(b))
        print("Of size " + str(c))
        self.assertEqual(b,3)
        self.assertEqual(c,4)
        self.assertEqual(a,bytearray(b'\x00\x00\x00\x03'))
        print("Class boolean")
        print(Element.decode(bytearray(b'\x00\x01\x00\x00\x01\x00\x00\x02\x00')))
        tcls,tval,tsize = Element.decode(bytearray(b'\x00\x01\x00\x00\x01\x00\x00\x02\x00'))
        self.assertEqual(tcls,Boolean)
        self.assertEqual(tval,False)
        self.assertEqual(tsize,8+1)

    @parameterized.expand([
        ["True", True, bytearray(b'\x01')],
        ["False", False, bytearray(b'\x00')],
    ])
    def test_boolean_encode(self,name,test_input,expected):
        e = Boolean(test_input)
        assert e.encode() == expected

    @parameterized.expand([
        ["True", bytearray(b'\x01'), True],
        ["False", bytearray(b'\x00'), False],
    ])
    def test_boolean_decode(self,name,test_input,expected):
        e = Boolean.decode(test_input)
        assert e == expected

    @parameterized.expand([
        ["True", True, bytearray(b'\x01\x01')],
        ["False", False, bytearray(b'\x01\x00')],
        ["None", None, bytearray(b'\x00')],
    ])
    def test_boolean_encode_as_nullable(self,name,test_input,expected):
        e = Nullable(Boolean).encode(test_input)
        assert e == expected

    @parameterized.expand([
        ["True", bytearray(b'\x01\x01'), True],
        ["False", bytearray(b'\x01\x00'), False],
        ["None", bytearray(b'\x00'), None],
    ])
    def test_boolean_decode_as_nullable(self,name,test_input,expected):
        e,sn = Nullable(Boolean).decode(test_input)
        assert e == expected

    @parameterized.expand([
        ["True", True, bytearray(b'\x01\x01')],
        ["False", False, bytearray(b'\x01\x00')],
    ])
    def test_boolean_encode_as_attribute(self,name,test_input,expected):
        e = Boolean.encode(test_input).as_attribute()
        assert e == expected

        # TODO: decode_as_attribute for Boolean

    @parameterized.expand([
        ["True", True, bytearray(b'\x00\x01\x00\x00\x01\x00\x00\x02\x01')],
        ["False", False, bytearray(b'\x00\x01\x00\x00\x01\x00\x00\x02\x00')],
    ])
    def test_boolean_encode_as_element(self,name,test_input,expected):
        e = Boolean.encode(test_input).as_element()
        assert e == expected

        # TODO: decode_as_element for Boolean

    @parameterized.expand([
        ["True", True, bytearray(b'\x00\x01\x00\x00\x01\x00\x00\x02\x01')],
        ["False", False, bytearray(b'\x00\x01\x00\x00\x01\x00\x00\x02\x00')],
    ])
    def test_boolean_encode_as_composite(self,name,test_input,expected):
        e = Boolean.encode(test_input).as_element()
        assert e == expected

        # TODO: decode_as_composite for Boolean

    @parameterized.expand([
        ["True", True, bytearray(b'\x00\x01\x00\x00\x01\x00\x00\x02\x01')],
        ["False", False, bytearray(b'\x00\x01\x00\x00\x01\x00\x00\x02\x00')],
    ])
    def test_boolean_encode_as_abstract(self,name,test_input,expected):
        e = Boolean.encode(test_input).as_element()
        assert e == expected

        # TODO: decode_as_abstract for Boolean

    @parameterized.expand([
        ["TrueFalse", [True, False] , bytearray(b'\x00\x00\x00\x02\x01\x01\x01\x00')],
        ["TrueNoneFalse", [True, None, False] , bytearray(b'\x00\x00\x00\x03\x01\x01\x00\x01\x00')],
    ])
    def test_boolean_encode_as_list(self,name,test_input,expected):
        l = List(Boolean)
        for v in test_input:
            l.append(v)
        el = l.encode()
        assert el == expected

    @parameterized.expand([
        ["ListTrueFalse", [True, False] , bytearray(b'\x01\x00\x00\x00\x02\x01\x01\x01\x00')],
        ["ListTrueNoneFalse", [True, None, False] , bytearray(b'\x01\x00\x00\x00\x03\x01\x01\x00\x01\x00')],
    ])
    def test_boolean_encode_as_nullable_list(self,name,test_input,expected):
        l = Nullable(List(Boolean))
        for v in test_input:
            l.append(v)
        el = l.encode()
        assert el == expected

    @parameterized.expand([
        ["TrueFalse", [True, False] , bytearray(b'\x00\x00\x00\x02\x01\x00\x01\x00\x00\x01\x00\x00\x02\x01\x01\x00\x01\x00\x00\x01\x00\x00\x02\x00')],
    ])
    def test_boolean_encode_as_element_list(self,name,test_input,expected):
        l = List(Element)
        for v in test_input:
            l.append(v,Boolean)
        el = l.encode()
        assert el == expected

    @parameterized.expand([
        ["0", 0, bytearray(b'\x00')],
        ["1", 1, bytearray(b'\x01')],
    ])
    def test_uoctet_encode(self,name,test_input,expected):
        e = UOctet(test_input)
        assert e.encode() == expected

    @parameterized.expand([
        ["0", bytearray(b'\x00'), 0],
        ["1", bytearray(b'\x01'), 1],
    ])
    def test_uoctet_decode(self,name,test_input,expected):
        e = UOctet.decode(test_input)
        assert e == expected

    @parameterized.expand([
        ["0", 0, bytearray(b'\x00')],
        ["1", 1, bytearray(b'\x01')],
        ["-1", -1, bytearray(b'\xFF')],
    ])
    def test_octet_encode(self,name,test_input,expected):
        e = Octet(test_input)
        assert e.encode() == expected

    @parameterized.expand([
        ["0", bytearray(b'\x00'), 0],
        ["1", bytearray(b'\x01'), 1],
        ["-1", bytearray(b'\xFF'), -1],
    ])
    def test_octet_decode(self,name,test_input,expected):
        e = Octet.decode(test_input)
        assert e == expected

    @parameterized.expand([
        ["0", 0, bytearray(b'\x00\x00')],
        ["1", 1, bytearray(b'\x00\x01')],
        ["256", 256, bytearray(b'\x01\x00')],
        ["65535", 65535, bytearray(b'\xFF\xFF')],
    ])
    def test_ushort_encode(self,name,test_input,expected):
        e = UShort(test_input)
        assert e.encode() == expected

    @parameterized.expand([
        ["0", bytearray(b'\x00\x00'), 0],
        ["1", bytearray(b'\x00\x01'), 1],
        ["256", bytearray(b'\x01\x00'), 256],
        ["65535", bytearray(b'\xFF\xFF'), 65535],
    ])
    def test_ushort_decode(self,name,test_input,expected):
        e = UShort.decode(test_input)
        assert e == expected

    @parameterized.expand([
        ["0", 0, bytearray(b'\x00\x00')],
        ["256", 256, bytearray(b'\x01\x00')],
        ["-1", -1, bytearray(b'\xFF\xFF')],
    ])
    def test_short_encode(self,name,test_input,expected):
        e = Short(test_input)
        assert e.encode() == expected

    @parameterized.expand([
        ["0", bytearray(b'\x00\x00'), 0],
        ["256", bytearray(b'\x01\x00'), 256],
        ["-1", bytearray(b'\xFF\xFF'), -1],
    ])
    def test_short_decode(self,name,test_input,expected):
        e = Short.decode(test_input)
        assert e == expected

    @parameterized.expand([
        ["0", 0, bytearray(b'\x00\x00\x00\x00')],
        ["65536", 65536, bytearray(b'\x00\x01\x00\x00')],
    ])
    def test_uinteger_encode(self,name,test_input,expected):
        e = UInteger(test_input)
        assert e.encode() == expected

    @parameterized.expand([
        ["0", bytearray(b'\x00\x00\x00\x00'), 0],
        ["65536", bytearray(b'\x00\x01\x00\x00'), 65536],
    ])
    def test_uinteger_decode(self,name,test_input,expected):
        e = UInteger.decode(test_input)
        assert e == expected

    @parameterized.expand([
        ["255", 255, bytearray(b'\x0B\x00\x00\x00\xFF')],
        ["65536", 65536, bytearray(b'\x0B\x00\x01\x00\x00')],
    ])
    def test_uinteger_encode_as_attribute(self,name,test_input,expected):
        e = UInteger.encode(test_input).as_attribute()
        assert e == expected

        # TODO: decode_as_attribute for UInteger

    @parameterized.expand([
        ["255", 255, bytearray(b'\x00\x01\x00\x00\x01\x00\x00\x0C\x00\x00\x00\xFF')],
        ["65536", 65536, bytearray(b'\x00\x01\x00\x00\x01\x00\x00\x0C\x00\x01\x00\x00')],
    ])
    def test_uinteger_encode_as_element(self,name,test_input,expected):
        e = UInteger.encode(test_input).as_element()
        assert e == expected

        # TODO: decode_as_element for UInteger

    @parameterized.expand([
        ["0", 0, bytearray(b'\x00\x00\x00\x00')],
        ["1", 1, bytearray(b'\x00\x00\x00\x01')],
        ["-1", -1, bytearray(b'\xFF\xFF\xFF\xFF')],
        ["-256", -256, bytearray(b'\xFF\xFF\xFF\x00')],
    ])
    def test_integer_encode(self,name,test_input,expected):
        e = Integer(test_input)
        assert e.encode() == expected

    @parameterized.expand([
        ["0", bytearray(b'\x00\x00\x00\x00'), 0],
        ["1", bytearray(b'\x00\x00\x00\x01'), 1],
        ["-1", bytearray(b'\xFF\xFF\xFF\xFF'), -1],
        ["-256", bytearray(b'\xFF\xFF\xFF\x00'), -256],
    ])
    def test_integer_decode(self,name,test_input,expected):
        e = Integer.decode(test_input)
        assert e == expected

    @parameterized.expand([
        ["0", 0, bytearray(b'\x00\x00\x00\x00\x00\x00\x00\x00')],
        ["65536", 65536, bytearray(b'\x00\x00\x00\x00\x00\x01\x00\x00')],
        ["0x0123456789abcdef", 0x0123456789abcdef, bytearray(b'\x01\x23\x45\x67\x89\xab\xcd\xef')],
    ])
    def test_ulong_encode(self,name,test_input,expected):
        e = ULong(test_input)
        assert e.encode() == expected

    @parameterized.expand([
        ["0", bytearray(b'\x00\x00\x00\x00\x00\x00\x00\x00'), 0],
        ["65536", bytearray(b'\x00\x00\x00\x00\x00\x01\x00\x00'), 65536],
        ["0x0123456789abcdef", bytearray(b'\x01\x23\x45\x67\x89\xab\xcd\xef'), 0x0123456789abcdef],
    ])
    def test_ulong_decode(self,name,test_input,expected):
        e = ULong.decode(test_input)
        assert e == expected

    @parameterized.expand([
        ["0", 0, bytearray(b'\x00\x00\x00\x00\x00\x00\x00\x00')],
        ["1", 1, bytearray(b'\x00\x00\x00\x00\x00\x00\x00\x01')],
        ["-1", -1, bytearray(b'\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF')],
        ["-256", -256, bytearray(b'\xFF\xFF\xFF\xFF\xFF\xFF\xFF\x00')],
        ["0x0123456789abcdef", 0x0123456789abcdef, bytearray(b'\x01\x23\x45\x67\x89\xab\xcd\xef')],
    ])
    def test_long_encode(self,name,test_input,expected):
        e = Long(test_input)
        assert e.encode() == expected

    @parameterized.expand([
        ["0", bytearray(b'\x00\x00\x00\x00\x00\x00\x00\x00'), 0],
        ["1", bytearray(b'\x00\x00\x00\x00\x00\x00\x00\x01'), 1],
        ["-1", bytearray(b'\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF'), -1],
        ["-256", bytearray(b'\xFF\xFF\xFF\xFF\xFF\xFF\xFF\x00'), -256],
        ["0x0123456789abcdef", bytearray(b'\x01\x23\x45\x67\x89\xab\xcd\xef'), 0x0123456789abcdef],
    ])
    def test_long_decode(self,name,test_input,expected):
        e = Long.decode(test_input)
        assert e == expected

    @parameterized.expand([
        ["0xDEADBEEF", bytearray(b'\xDE\xAD\xBE\xEF'), bytearray(b'\x00\x00\x00\x04\xDE\xAD\xBE\xEF')],
    ])
    def test_blob_encode(self,name,test_input,expected):
        e = Blob(test_input)
        assert e.encode() == expected

    @parameterized.expand([
        ["0xDEADBEEF", bytearray(b'\x00\x00\x00\x04\xDE\xAD\xBE\xEF'), bytearray(b'\xDE\xAD\xBE\xEF')],
    ])
    def test_blob_decode(self,name,test_input,expected):
        e = Blob.decode(test_input)
        assert e == expected

    @parameterized.expand([
        ["1", 1, bytearray(b'\x3f\x80\x00\x00')],
        ["3.141590118408203125", 3.141590118408203125, bytearray(b'\x40\x49\x0f\xd0')],
        ["-6259853398707798016", -6259853398707798016, bytearray(b'\xde\xad\xbe\xef')],
    ])
    def test_float_encode(self,name,test_input,expected):
        e = Float(test_input)
        assert e.encode() == expected

    @parameterized.expand([
        ["1", bytearray(b'\x3f\x80\x00\x00'), 1],
        ["3.141590118408203125", bytearray(b'\x40\x49\x0f\xd0'), 3.141590118408203125],
        ["-6259853398707798016", bytearray(b'\xde\xad\xbe\xef'), -6259853398707798016],
    ])
    def test_float_decode(self,name,test_input,expected):
        e = Float.decode(test_input)
        assert e == expected

    @parameterized.expand([
        ["1", 1, bytearray(b'\x3F\xF0\x00\x00\x00\x00\x00\x00')],
        ["3.14159200000000016217427400989", 3.14159200000000016217427400989, bytearray(b'\x40\x09\x21\xFA\xFC\x8B\x00\x7A')],
        ["-1.18859592646101991673607120446E148", -1.18859592646101991673607120446E148, bytearray(b'\xDE\xAD\xBE\xEF\xDE\xFE\xC8\xED')],
    ])
    def test_double_encode(self,name,test_input,expected):
        e = Double(test_input)
        assert e.encode() == expected

    @parameterized.expand([
        ["1", bytearray(b'\x3F\xF0\x00\x00\x00\x00\x00\x00'), 1],
        ["3.14159200000000016217427400989", bytearray(b'\x40\x09\x21\xFA\xFC\x8B\x00\x7A'), 3.14159200000000016217427400989],
        ["-1.18859592646101991673607120446E148", bytearray(b'\xDE\xAD\xBE\xEF\xDE\xFE\xC8\xED'), -1.18859592646101991673607120446E148],
    ])
    def test_double_decode(self,name,test_input,expected):
        e = Double.decode(test_input)
        assert e == expected

    @parameterized.expand([
        ["1", 1, bytearray(b'\x3F\xF0\x00\x00\x00\x00\x00\x00')],
        ["3.14159200000000016217427400989", 3.14159200000000016217427400989, bytearray(b'\x40\x09\x21\xFA\xFC\x8B\x00\x7A')],
        ["-1.18859592646101991673607120446E148", -1.18859592646101991673607120446E148, bytearray(b'\xDE\xAD\xBE\xEF\xDE\xFE\xC8\xED')],
    ])
    def test_duration_encode(self,name,test_input,expected):
        e = Duration(test_input)
        assert e.encode() == expected

    @parameterized.expand([
        ["1", bytearray(b'\x3F\xF0\x00\x00\x00\x00\x00\x00'), 1],
        ["3.14159200000000016217427400989", bytearray(b'\x40\x09\x21\xFA\xFC\x8B\x00\x7A'), 3.14159200000000016217427400989],
        ["-1.18859592646101991673607120446E148", bytearray(b'\xDE\xAD\xBE\xEF\xDE\xFE\xC8\xED'), -1.18859592646101991673607120446E148],
    ])
    def test_duration_decode(self,name,test_input,expected):
        e = Duration.decode(test_input)
        assert e == expected

    @parameterized.expand([
        ["'This_is_a_string'", 'This_is_a_string', bytearray(b'\x00\x00\x00\x10\x54\x68\x69\x73\x5f\x69\x73\x5f\x61\x5f\x73\x74\x72\x69\x6e\x67')],
    ])
    def test_string_encode(self,name,test_input,expected):
        e = String(test_input)
        assert e.encode() == expected

    @parameterized.expand([
        ["'This_is_a_string'", bytearray(b'\x00\x00\x00\x10\x54\x68\x69\x73\x5f\x69\x73\x5f\x61\x5f\x73\x74\x72\x69\x6e\x67'), 'This_is_a_string'],
    ])
    def test_string_decode(self,name,test_input,expected):
        e = String.decode(test_input)
        assert e == expected

    @parameterized.expand([
        ["0.128", 0.128, bytearray(b'\x11\x1F\x00\x00\x00\x80')],
        ["86400.512", 86400.512, bytearray(b'\x11\x20\x00\x00\x02\x00')],
    ])
    def test_time_encode(self,name,test_input,expected):
        e = Time(test_input)
        assert e.encode() == expected

    @parameterized.expand([
        ["0.128", bytearray(b'\x11\x1F\x00\x00\x00\x80'), 0.128],
        ["86400.512", bytearray(b'\x11\x20\x00\x00\x02\x00'), 86400.512],
    ])
    def test_time_decode(self,name,test_input,expected):
        e = Time.decode(test_input)
        assert e == expected

    @parameterized.expand([
        ["Decimal('0.128000065536')", Decimal('0.128000065536'), bytearray(b'\x11\x1F\x00\x00\x00\x80\x00\x01\x00\x00')],
    ])
    def test_finetime_encode(self,name,test_input,expected):
        e = FineTime(test_input)
        assert e.encode() == expected

    @parameterized.expand([
        ["Decimal('0.128000065536')", bytearray(b'\x11\x1F\x00\x00\x00\x80\x00\x01\x00\x00'), Decimal('0.128000065536')],
    ])
    def test_finetime_decode(self,name,test_input,expected):
        e = FineTime.decode(test_input)
        assert e == expected

if __name__ == '__main__':
    unittest.main()
